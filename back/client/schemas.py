from pydantic import BaseModel, validator
from datetime import datetime
import re

class ClientBase(BaseModel):
    phone : str
    phone: str
    @validator("phone")
    def phone_validation(cls, v):
        regex = r"7[0-9]{10}$"
        if v and not re.search(regex, v, re.I):
            raise ValueError("Phone Number Invalid.")
        return v
    code : str
    tag : str
    timezone : str

class Client(ClientBase):
    id: int
    is_active: bool

    class Config:
        orm_mode = True
