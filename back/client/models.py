from database import Base
from sqlalchemy import (
    Boolean,
    Column,
    ForeignKey,
    Integer,
    String,
    DateTime,
    PickleType
)

class Client(Base):
    __tablename__ = "client"

    id = Column(Integer, primary_key=True, index=True)
    is_active = Column(Boolean, default=True)
    phone = Column(String)
    code = Column(String)
    tag = Column(String)
    timezone = Column(String)
