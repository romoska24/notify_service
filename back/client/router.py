from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from . import schemas, crud
from dependencies import get_db

router = APIRouter(
    prefix="/client",
    tags=["client"],
    responses={404: {"description": "Not found"}},
)

@router.post("/", response_model=schemas.Client)
def create_client(client: schemas.ClientBase, db: Session = Depends(get_db)):
    return crud.create_client(db, client)

@router.post("/update/{client_id}", response_model=schemas.Client)
def update_client(client_id: int, kwargs: schemas.ClientBase, db: Session = Depends(get_db)):
    if not (client:=crud.get_client_by_id(db, client_id)):
        raise HTTPException(status_code=400, detail="Client doesnt exists")
    return crud.update_client(db, client, kwargs)

@router.post("/delete/{client_id}")
def delete_client(client_id: int, db: Session = Depends(get_db)):
    if not (client:=crud.get_client_by_id(db, client_id)):
        raise HTTPException(status_code=400, detail="Client doesnt exists")
    return crud.delete_client(db, client)
