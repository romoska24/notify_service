from sqlalchemy.orm import Session

from . import models, schemas


def create_client(db: Session, client: schemas.Client):
    db_client = models.Client(
        phone = client.phone,
        code = client.code,
        tag = client.tag,
        timezone = client.timezone,
    )
    db.add(db_client)
    db.commit()
    db.refresh(db_client)
    return db_client

def get_client_by_id(db: Session, client_id: int):
    return db.query(models.Client).filter_by(id=client_id, is_active=True).first()

def update_client(db: Session, client: models.Client, kwargs: schemas.ClientBase):
    for attr_name, value in dict(kwargs).items():
        setattr(client, attr_name, value)

    db.commit()
    db.refresh(client)
    return client

def delete_client(db: Session, client: models.Client):
    client.is_active = False

    db.commit()
    db.refresh(client)
    return client
