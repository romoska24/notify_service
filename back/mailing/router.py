from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from . import schemas, crud
from mail.schemas import Mail
from dependencies import get_db

router = APIRouter(
    prefix="/mailing",
    tags=["mailing"],
    responses={404: {"description": "Not found"}},
)

@router.post("/", response_model=schemas.Mailing)
def create_mailing(mailing: schemas.MailingBase, db: Session = Depends(get_db)):
    return crud.create_mailing(db, mailing)

@router.get("/", response_model=list[schemas.Mailing])
def read_mailings(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    mailings = crud.get_mailings(db, skip, limit)
    return mailings

@router.get("/{mailing_id}", response_model=list[schemas.Mail])
def read_mails_by_mailing_id(mailing_id: int, db: Session = Depends(get_db)):
    if not (mails := crud.get_mails_by_mailing_id(db, mailing_id)) :
        raise HTTPException(status_code=404, detail="Mails not found")
    return mails
