from pydantic import BaseModel
from datetime import datetime
from mail.schemas import Mail

class MailingBase(BaseModel):
    d_start : datetime
    mail : str
    filter : dict
    d_end : datetime

class Mailing(MailingBase):
    id: int
    is_active: bool
    mails: list[Mail]

    class Config:
        orm_mode = True
