from database import Base
from sqlalchemy import (
    Boolean,
    Column,
    ForeignKey,
    Integer,
    String,
    DateTime,
    PickleType,
)
from sqlalchemy.orm import relationship


class Mailing(Base):
    __tablename__ = "mailing"

    id = Column(Integer, primary_key=True, index=True)
    is_active = Column(Boolean, default=True)
    d_start = Column(DateTime)
    mail = Column(String)
    filter = Column(PickleType)
    d_end = Column(DateTime)

    mails = relationship("Mail", backref="mailing")
