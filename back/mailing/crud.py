from sqlalchemy.orm import Session

from . import models, schemas


def create_mailing(db: Session, mailing: schemas.Mailing):
    db_mailing = models.Mailing(
        d_start = mailing.d_start,
        mail = mailing.mail,
        filter = mailing.filter,
        d_end = mailing.d_end,
    )
    db.add(db_mailing)
    db.commit()
    db.refresh(db_mailing)
    return db_mailing

def get_mailings(db: Session, skip: int = 0, limit: int = 10):
    return db.query(models.Mailing).offset(skip).limit(limit).all()

def get_mails_by_mailing_id(db: Session, mailing_id: int):
    return db.query(models.Mailing).get(mailing_id).mails
