from fastapi import FastAPI

from mailing.router  import router as mailing_router
from mail.router  import router as mail_router
from client.router  import router as client_router



# models.Base.metadata.create_all(bind=engine)

app = FastAPI()

app.include_router(mailing_router)
app.include_router(mail_router)
app.include_router(client_router)
