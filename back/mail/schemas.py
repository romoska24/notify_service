from pydantic import BaseModel
from datetime import datetime

class MailBase(BaseModel):
    d_create : datetime
    status : str
    mailing_id : int
    client_id : int

class Mail(MailBase):
    id: int

    class Config:
        orm_mode = True
