from sqlalchemy.orm import Session

from . import models, schemas


def create_mail(db: Session, mail: schemas.Mail):
    db_mail = models.Mail(
        d_create = mail.d_create,
        status = mail.status,
        mailing_id = mail.mailing_id,
        client_id = mail.client_id,
    )
    db.add(db_mail)
    db.commit()
    db.refresh(db_mail)
    return db_mail
