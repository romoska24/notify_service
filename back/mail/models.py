from database import Base
from sqlalchemy import (
    Boolean,
    Column,
    ForeignKey,
    Integer,
    String,
    DateTime,
    PickleType
)

class Mail(Base):
    __tablename__ = "mail"

    id = Column(Integer, primary_key=True, index=True)
    d_create = Column(DateTime)
    status = Column(String)

    mailing_id = Column(Integer, ForeignKey("mailing.id"))
    client_id = Column(Integer, ForeignKey("client.id"))
